package ru.reboot.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.reboot.dto.ItemInfo;
import ru.reboot.service.AdminService;

import java.util.Date;
import java.util.List;

/**
 * Price controller.
 */
@RestController
@RequestMapping(path = "admin")
public class AdminControllerImpl implements AdminController {

    private static final Logger logger = LogManager.getLogger(AdminControllerImpl.class);

    private AdminService adminService;

    @Autowired
    public void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping("info")
    public String info() {
        logger.info("method .info invoked");
        return "AdminController " + new Date();
    }

    @Override
    public List<ItemInfo> getAllItems() {
        return adminService.getAllItems();
    }

    @Override
    public void setPrice(String itemId, double price) {
        adminService.setPrice(itemId, price);
    }
}
