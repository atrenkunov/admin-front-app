package ru.reboot.controller;

import ru.reboot.dto.ItemInfo;

import java.util.List;

public interface AdminController {

    /**
     * Get all items.
     */
    List<ItemInfo> getAllItems();

    /**
     * Set item price.
     *
     * @param itemId - item id
     * @param price  - new price
     */
    void setPrice(String itemId, double price);
}
