
package ru.reboot.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.reboot.dto.ItemInfo;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class AdminService {

    private static final Logger logger = LogManager.getLogger(AdminService.class);

    private ItemCache itemCache;
    private PriceCache priceCache;

    @Autowired
    public void setItemCache(ItemCache itemCache) {
        this.itemCache = itemCache;
    }

    @Autowired
    public void setPriceCache(PriceCache priceCache) {
        this.priceCache = priceCache;
    }

    /**
     * Set item price.
     * We should send request to price-service
     *
     * @param itemId - item id
     * @param price  - item price
     */
    public void setPrice(String itemId, double price) {
    }

    /**
     * Get all items.
     */
    public List<ItemInfo> getAllItems() {
        return null;
    }

    /**
     * Init cache here.
     */
    @PostConstruct
    public void init() {

        initializePriceCache();
        initializeItemCache();
    }

    private void initializePriceCache() {
    }

    private void initializeItemCache() {
    }
}
