package ru.reboot.error;

public class ErrorCode {

    public static final String ILLEGAL_ARGUMENT = "ILLEGAL_ARGUMENT";
    public static final String PRICE_NOT_FOUND = "PRICE_NOT_FOUND";
}
